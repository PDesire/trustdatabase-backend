package de.trustdatabase.backend.mapping

import de.trustdatabase.backend.projectzerotwo.types.exception.RequestValueException
import de.trustdatabase.backend.projectzerotwo.types.vehicle.Vehicle
import de.trustdatabase.backend.projectzerotwo.types.vehicle.VehicleController
import org.jetbrains.annotations.NotNull
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.Instant
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

@RestController
class VehicleRequest {

    @GetMapping("/vehicle")
    fun vehicle(@RequestParam(value = "vehicle", defaultValue = "none") train: String,
                @RequestParam(value = "time", defaultValue = "none") time: String,
                @RequestParam(value = "direction", defaultValue = "none") direction: String,
                @RequestParam(value = "type", defaultValue = "none") type: String,
                @RequestParam(value = "privacy", defaultValue = "none") privacy: String) =
            requestHandler(train, time, direction, type, privacy)


    fun requestHandler(vehicle: String, time: String, direction: String, type: String, privacy: String) : Vehicle{
        @NotNull val vehicleController = VehicleController()

        if (vehicle == "none" || time == "none" || direction == "none" || type == "none" || privacy == "none")
            throw RequestValueException(DateTimeFormatter
                    .ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS")
                    .withZone(ZoneOffset.UTC)
                    .format(Instant.now()))

        if (privacy == "false") sendStatistics()

        return vehicleController.getVehicle(vehicle, time, direction, type).content
    }

    fun sendStatistics() {
        // TODO: create Firebase statistics collection
    }
}