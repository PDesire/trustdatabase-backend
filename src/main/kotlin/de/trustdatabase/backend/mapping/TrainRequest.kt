package de.trustdatabase.backend.mapping

import de.trustdatabase.backend.projectzerotwo.types.exception.RequestValueException
import de.trustdatabase.backend.projectzerotwo.types.train.Train
import de.trustdatabase.backend.projectzerotwo.types.train.TrainController
import org.jetbrains.annotations.NotNull
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.Instant
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

@RestController
class TrainRequest {

    @GetMapping("/train")
    fun train(@RequestParam(value = "train", defaultValue = "none") train: String,
              @RequestParam(value = "time", defaultValue = "none") time: String,
              @RequestParam(value = "privacy", defaultValue = "none") privacy: String) =
            requestHandler(train, time, privacy)


    fun requestHandler(train: String, time: String, privacy: String) : Train {
        @NotNull val trainController = TrainController()

        if (train == "none" || time == "none" || privacy == "none")
            throw RequestValueException(DateTimeFormatter
                    .ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS")
                    .withZone(ZoneOffset.UTC)
                    .format(Instant.now()))

        if (privacy == "false") sendStatistics()

        return trainController.getTrain(train, time).content
    }

    fun sendStatistics() {
        // TODO: create Firebase statistics collection
    }
}