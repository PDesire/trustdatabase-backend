package de.trustdatabase.backend.projectzerotwo.trustframework

class TrustLevelGenerator(private val positive_requests: Int, private val negative_requests: Int) {

    fun getTrustLevel() : String {
        if (positive_requests == 0) return zeroDesicionNegative()
        if (negative_requests == 0) return zeroDesicionPositive()

        val numdesicion: Double = positive_requests.toDouble() / negative_requests.toDouble()

        return when {
            numdesicion < 1 -> badly(numdesicion)
            numdesicion.toInt() == 1 -> TrustLevels.TRUSTLEVEL_OKAY.value
            else -> goodly(numdesicion)
        }

    }

    private fun badly(num: Double) : String {
        return when {
            num > 0.4 -> TrustLevels.TRUSTLEVEL_VERY_BAD.value
            num > 0.7 -> TrustLevels.TRUSTLEVEL_BAD.value
            num > 0.9 -> TrustLevels.TRUSTLEVEL_OKAY.value
            else -> TrustLevels.TRUSTLEVEL_UNUSABLE.value
        }
    }

    private fun goodly(num: Double) : String {
        return when {
            num > 2 -> TrustLevels.TRUSTLEVEL_PERFECT.value
            num > 1.5 -> TrustLevels.TRUSTLEVEL_VERY_GOOD.value
            num > 1.2 -> TrustLevels.TRUSTLEVEL_GOOD.value
            else -> TrustLevels.TRUSTLEVEL_OKAY.value
        }
    }


    private fun zeroDesicionNegative() : String {
        return when {
            negative_requests > 750 -> TrustLevels.TRUSTLEVEL_UNUSABLE.value
            negative_requests > 500 -> TrustLevels.TRUSTLEVEL_VERY_BAD.value
            negative_requests > 100 -> TrustLevels.TRUSTLEVEL_BAD.value
            negative_requests > 50 -> TrustLevels.TRUSTLEVEL_OKAY.value
            else -> TrustLevels.TRUSTLEVEL_NOT_VOTED.value
        }
    }

    private fun zeroDesicionPositive() : String {
        return when {
            positive_requests > 750 -> TrustLevels.TRUSTLEVEL_PERFECT.value
            positive_requests > 500 -> TrustLevels.TRUSTLEVEL_VERY_GOOD.value
            positive_requests > 100 -> TrustLevels.TRUSTLEVEL_GOOD.value
            positive_requests > 50 -> TrustLevels.TRUSTLEVEL_OKAY.value
            else -> TrustLevels.TRUSTLEVEL_NOT_VOTED.value
        }
    }
}