package de.trustdatabase.backend.projectzerotwo.trustframework

class TrustGenerator(private val positive_requests: Int, private val negative_requests: Int) {

    fun getTrustLevel() : String {
        val generator = TrustLevelGenerator(positive_requests, negative_requests)

        return generator.getTrustLevel()
    }

    fun getTrusts() : String {
        val summed = positive_requests + negative_requests

        return if (summed > 150)
            highVotes()
        else
            lowVotes()

    }

    private fun lowVotes() : String {

        return if (positive_requests > negative_requests) {

            if (positive_requests > negative_requests + 50)
                Trusts.TRUSTS_YES.value
            else
                Trusts.TRUSTS_MAYBE.value

        } else if (positive_requests == negative_requests){

            Trusts.TRUSTS_MAYBE.value

        } else {

            if (positive_requests + 50 > negative_requests)
                Trusts.TRUSTS_MAYBE.value
            else
                Trusts.TRUSTS_NO.value
        }
    }

    private fun highVotes() : String {
        return if (positive_requests > negative_requests) {

            if (positive_requests > negative_requests + 150)
                Trusts.TRUSTS_YES.value
            else
                Trusts.TRUSTS_MAYBE.value

        } else if (positive_requests == negative_requests){

            Trusts.TRUSTS_MAYBE.value

        } else {

            if (positive_requests + 150 > negative_requests)
                Trusts.TRUSTS_MAYBE.value
            else
                Trusts.TRUSTS_NO.value
        }
    }
}