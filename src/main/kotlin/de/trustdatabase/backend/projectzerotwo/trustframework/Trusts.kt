package de.trustdatabase.backend.projectzerotwo.trustframework

enum class Trusts (val value: String) {
    TRUSTS_YES("yes"),
    TRUSTS_MAYBE("maybe"),
    TRUSTS_NO("no")
}