package de.trustdatabase.backend.projectzerotwo.trustframework

enum class TrustLevels (val value: String) {
    TRUSTLEVEL_UNUSABLE("unusable"),
    TRUSTLEVEL_VERY_BAD("very bad"),
    TRUSTLEVEL_BAD("bad"),
    TRUSTLEVEL_OKAY("okay"),
    TRUSTLEVEL_GOOD("good"),
    TRUSTLEVEL_VERY_GOOD("very good"),
    TRUSTLEVEL_PERFECT("perfect"),
    TRUSTLEVEL_NOT_VOTED("not voted")
}