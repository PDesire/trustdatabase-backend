package de.trustdatabase.backend.projectzerotwo.types.vehicle

import de.trustdatabase.backend.projectzerotwo.trustframework.TrustGenerator
import org.json.JSONObject
import com.google.gson.Gson

class VehicleController {


    fun getVehicle(vehicle: String, time: String, direction: String, type: String) : VehicleResponse {
        val json = JSONObject()

        // Stub Values
        val positive_requests = 700
        val negative_requests = 500

        val trust = TrustGenerator(positive_requests, negative_requests)


        json.put("trustframework", trust.getTrusts())
        json.put("trustlevel", trust.getTrustLevel())
        json.put("vehicle", vehicle)
        json.put("direction", direction)
        json.put("type", type)
        json.put("time", time)

        return VehicleResponse(resolveJSON(json))
    }

    fun resolveJSON(json: JSONObject) : Vehicle {
        val gson = Gson()

        return gson.fromJson(json.toString(), Vehicle::class.java)
    }

}