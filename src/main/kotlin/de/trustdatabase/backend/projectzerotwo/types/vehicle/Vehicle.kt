package de.trustdatabase.backend.projectzerotwo.types.vehicle

data class Vehicle (val trust: String,
                    val trustlevel: String,
                    val vehicle: String,
                    val direction: String,
                    val time: String)