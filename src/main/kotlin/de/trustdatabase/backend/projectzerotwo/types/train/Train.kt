package de.trustdatabase.backend.projectzerotwo.types.train

data class Train (val trust: String, val trustlevel: String, val train: String, val time: String)