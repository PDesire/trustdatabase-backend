package de.trustdatabase.backend.projectzerotwo.types.train

import de.trustdatabase.backend.projectzerotwo.trustframework.TrustGenerator
import org.json.JSONObject
import com.google.gson.Gson

class TrainController {


    fun getTrain(train: String, time: String) : TrainResponse {
        val json = JSONObject()

        // Stub Values
        val positive_requests = 700
        val negative_requests = 500

        val trust = TrustGenerator(positive_requests, negative_requests)


        json.put("trustframework", trust.getTrusts())
        json.put("trustlevel", trust.getTrustLevel())
        json.put("train", train)
        json.put("time", time)

        return TrainResponse(resolveJSON(json))
    }

    fun resolveJSON(json: JSONObject) : Train {
        val gson = Gson()

        return gson.fromJson(json.toString(), Train::class.java)
    }

}