package de.trustdatabase.backend.projectzerotwo.types.train

data class TrainResponse(val content: Train)
