package de.trustdatabase.backend.projectzerotwo.types.vehicle

data class VehicleResponse(val content: Vehicle)
